# TRUCKS CARGO CALCULATOR

## DEVELOPMENT TIME : 6 hours

## TASK

Logistics company wants to put packages from customer's warehouse onto trucks. 
The only way to measure the packages is their mass in kilograms (maximum 500), being a natural number. 
Truck's maximum load is 1000 kg. 
Make an app that will say how many trucks are needed to move client's cargo (try to use as few as possible) and calculate the price.

Tasks: 
Make a public HTTP API who will get a request with body like:

```
[
  { "id": "ID-1", "weight": 345 },
  { "id": "OTHER-ID-2", "weight": 500 },
  { "id": "CLIENT-ID-3", "weight": 300 },
]
```

And reply with a response like:
```
{
  "price": 10.95,
  "trucks": [
    {
      "truckID": "unique truck ID",
      "load": [
        { "id": "ID-1", "weight": 345 },
        { "id": "OTHER-ID-2", "weight": 500 }
      ]
    },
    {
      "truckID": "other unique truck ID",
      "load": [
        { "id": "CLIENT-ID-3", "weight": 300 }
      ]
    }
  ]
}
```


## USAGE

### Config app

Configure database (mongoDB) connection. You can configure db url in file:

```
/config.js
```

in property

```
config.db.url
```

### Run server

```
node main
```

Server starts on port 9999.
Example path to available resources runing on localhost
```
http://localhost:9999/cargo/prices
```


## REST API
| METHOD 	|  	| PATH 	|  	| DESCRIPTION 	|  	| EXAMPLE INPUT 	|
|---	|---	|---	|---	|---	|---	|---	|
| POST 	| 	 	| /cargo/estimate  |	| Estimates cargo price and trucks needed. 	|	 | ```[{ "id": "ID-1", "weight": 345 },{ "id": "OTHER-ID-2", "weight": 500 },{ "id": "CLIENT-ID-3", "weight": 300 }]```  | 
| POST 	| 	 	| /order/all  |	| Returns JSON Array with all requests with estimated orders. 	|	 |   | 
| POST 	| 	 	| /order/one/:id  |	| Returns JSON object with requested estimation. 	|	 | 5b6c7c16f32c902690405972  | 

## STATIC PAGES
| METHOD 	|  	| PATH 	|  	| DESCRIPTION 	|  
|---	|---	|---	|---	|---	|---
| GET 	| 	 	| /cargo/prices  |	| Prices listy pdf. 	|