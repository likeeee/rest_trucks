const ObjectId = require('mongodb').ObjectID;
const config =  require('../config');


function save(requestBody, order){
	let data = {
		time: new Date(),
		request: requestBody,
		order: order
	};

	let db = config.db.connection;				

	if(!db)
		return console.log('Error while database connecting');

    db.collection(config.db.collectionOrders, (err, collection) => {
        
        collection.insert(data);

    });
}

function getAll(callback) {
	let db = config.db.connection;			

	if(!db){
		if(typeof callback === 'function')
			callback(err, data)
		return;
	}

	db.collection(config.db.collectionOrders, (err, collection) => {
	    
	    collection.find().toArray((err, data) => {
			if(typeof callback === 'function')
				return callback(err, data)
	    });

	});
}

function getOne(id, callback) {
	let db = config.db.connection;		

	if(!db){
		if(typeof callback === 'function')
			callback(err, data)
		return;
	}


    db.collection(config.db.collectionOrders, (err, collection) => {
		try{	
	        
	        collection.findOne({"_id": new ObjectId(id)}, (err, data) => {
				if(typeof callback === 'function')
					return callback(err, data);
	        });

		} catch (e) {
			if(typeof callback === 'function')
				return callback(true);
		}

    });
}



module.exports = {
	save: save,
	getAll: getAll,
	getOne: getOne	
}