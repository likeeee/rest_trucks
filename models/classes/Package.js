const PACKAGE_MAX_WEIGHT = 500;
const PACKAGE_PRICE_TRESHOLD = 400;
const PRICE_FACTOR_UP_TO_400 = 0.01;
const PRICE_FACTOR_OVER_400 = 0.005;

class Package{

	constructor(id, weight){
		this.id = id;
		this.weight = weight;
	}

	isValid(weight){
		return Number.isInteger( + this.weight );
	}

	getPrice(){
		if(!this.isValid())
			return null;

		switch( this.weight <= PACKAGE_PRICE_TRESHOLD){
			case true:
				return this.weight * PRICE_FACTOR_UP_TO_400;
			case false:
				return this.weight * PRICE_FACTOR_OVER_400 + 2;
		}
	}

	static getMaxWeight(){
		return PACKAGE_MAX_WEIGHT;
	}
}

module.exports = Package;