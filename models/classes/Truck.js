const TRUCK_MAX_LOAD = 1000;

class Truck {
	constructor(id){
		this.id = id;
		this.load = [];
	}

	getCurrentLoad(){
		return this.load.reduce(
			(currentSum, pckg) => {
				return currentSum + pckg.weight;
			}, 0);
	}

	loadPackage(pckg){
		this.load.push(pckg);
	}

	static getMaxLoad(){
		return TRUCK_MAX_LOAD;
	}
}

module.exports = Truck;