const Truck = require('./Truck');
const Package = require('./Package');

class CargoCollection{
	constructor(){
		this.packages = [];
		this.packagesOverweight = [];
		this.trucks = [];

		this.price = 0;
	}

	addPackage(pckg){
		if(!(pckg instanceof Package))
			return;

		if(pckg.weight > Truck.getMaxLoad())
			return this.packagesOverweight.push(pckg);

		this.packages.push(pckg);
		this.price += pckg.getPrice();
		this.sort();
	}

	addPackages(packages){
		Array.prototype.push.apply(this.packages, packages);
		this.sort();
	}

	createVehicle(){
		let truck = new Truck(this.trucks.length + 1);

		// Save truck
		this.trucks.push(truck);

		return truck;
	}

	getLoad(){
		return {
			price: this.price,
			trucks: Object.assign({}, this.trucks),
			errors: {}
		}
	}

	fitPackages(){
		if(!this.packages.length)
			return;

		let cTruck;
		let found;

		if(!this.trucks.length){
			cTruck = this.createVehicle();
		}

		// Add first package to truck
		cTruck.loadPackage(this.packages.shift());

		//found = this.findBestFittingPackage(cTruck);

		while(this.packages.length){
			found = this.findBestFittingPackage(cTruck);

			// Package found
			if(found){
				cTruck.loadPackage(found);
				continue;
			}

			// Package not found we need to create new vehicle
			cTruck = this.createVehicle();

			if(this.packages.length)
				cTruck.loadPackage(this.packages.shift());
		}
	}

	findBestFittingPackage(truck){
		let lastWeight = Truck.getMaxLoad() - truck.getCurrentLoad();
		let lastTruckWeight;
		let bestLastTruckWeight;
		let bestPackageIndex;
		let cPackage;
		let pckg;


		for(let i = 0; i < this.packages.length; i++){
			cPackage = this.packages[i];
			lastTruckWeight = lastWeight - cPackage.weight; 


			// If lower than zero, continue
			if(lastTruckWeight < 0)
				continue;

			if(!Number.isInteger(bestPackageIndex)){
				bestLastTruckWeight = lastTruckWeight;
				bestPackageIndex = i;
				continue;
			}

			// Check if is better
			if(lastTruckWeight < bestLastTruckWeight){
				bestLastTruckWeight = lastTruckWeight;
				bestPackageIndex = i;
			}
		}

		// No best package
		if(!Number.isInteger(bestPackageIndex))
			return;

		// Return best backage and remove from container
		pckg = this.packages[bestPackageIndex];
		this.packages.splice(bestPackageIndex, 1);

		return pckg;
	}

	totalWeight(){
		return this.packages.reduce(
			(currentSum, pckg) => {
				if(!pckg.isValid())
					return currentSum;

				return currentSum + pckg.weight;
			}, 0);
	}

	sort(){
		this.packages.sort((a, b) => {
			return b.weight - a.weight;
		});
	}

}

module.exports = CargoCollection;