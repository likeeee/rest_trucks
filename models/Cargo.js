const CargoCollection = require('./classes/CargoCollection');
const Package = require('./classes/Package');


function mapPackages(data) {
	if(!Array.isArray(data))
		return 0;

	let packages = [];
	let packagesIncorrectInput = [];
	let packagesWeightExceeded = [];
	let out = {};

	data.forEach((item) => {
		if( !item.hasOwnProperty('id') || !item.hasOwnProperty('weight') ){
			item.passedPropertyTest_id = item.hasOwnProperty('id');
			item.passedPropertyTest_weight = item.hasOwnProperty('weight');

			packagesIncorrectInput.push(item);
			return;
		}

		if(item.weight > Package.getMaxWeight()){
			// Set max weight
			item.maxWeight = Package.getMaxWeight();

			return packagesWeightExceeded.push(item);
		}

		packages.push(new Package(item.id, item.weight));
	});

	return {
		packages: packages,
		errors: {
			packagesWeightExceeded: packagesWeightExceeded,
			packagesIncorrectInput: packagesIncorrectInput
		}
	};
}

function load(packages) {
	let cargo = new CargoCollection();

	packages.forEach((pckg) => {
		cargo.addPackage(pckg); 
	});

	cargo.fitPackages();

	return cargo.getLoad();
}


module.exports = {
	mapPackages: mapPackages,
	load: load
}