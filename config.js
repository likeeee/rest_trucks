const MongoClient = require('mongodb').MongoClient;

config = {
	app: {
		port: 9999
	},
	db:{
		url: 'mongodb://localhost:27017',
		name: 'Trucks',
		collectionOrders: 'orders',
		connection: null,
		connect: connect
	}

}


function connect(callback) {
	if(typeof callback !== 'function')
		throw new Error('Cannot start application because config.db.connect is not a callback function');

	MongoClient.connect(config.db.url, (err, database) => {

		if(err){
			return callback(err);
		}

		let db = database.db(config.db.name);

		config.db.connection = db;
	                

	    callback();
	});
}

module.exports = config;