const express = require('express')
const morgan = require('morgan');

const app = express()
const config = require('./config');
const app_routes = require('./controllers/routes.js');

// USE middlewares
app.use(express.json());

// REQ Logging
app.use(morgan('combined'));

// Apply routes
app.use('/', app_routes);


config.db.connect((err) => {
	if(err)
		return console.log('Cannot start APP because of problem with db connection');	

	app.listen(config.app.port, () => console.log(`TRUCKS REST API listening on port ${config.app.port}!`));
	
});