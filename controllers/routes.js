const express = require('express');
const router = express.Router();

// Controllers
const controllerCargo = require('./cargo.js');
const controllerOrder = require('./order.js');


// Use controllers routes
router.use('/cargo', controllerCargo);
router.use('/order', controllerOrder);


module.exports = router;