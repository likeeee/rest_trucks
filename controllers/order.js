const express = require('express');
const router = express.Router();

// Models
const Order = require('../models/Order');

const routes = {};

/*
 * ROUTES DEFINITION
 */

routes.all = (req, res) => {
	Order.getAll((err, data) => {
		if(err){
			res.status(500);
			res.end('Inernal server error');
			return;
		}

		res.json(data);
	});
}
//5b6c7c16f32c902690405972
routes.one = (req, res) => {

	let id = req.params.id;


		Order.getOne(id, (err, data) => {
			if(err){
				res.status(500);
				res.end('Internal server error');
				return;
			}

			res.json(data);
		});
}

/*
 * ROUTES BINDING
 */
router.get('/all', routes.all);
router.get('/one/:id', routes.one);


module.exports = router;