const express = require('express');
const fs = require('fs');
const path = require('path');
const router = express.Router();
const PDF_PATH_PRICES = path.join(__dirname, '../public/prices.pdf');

// Models
const Cargo = require('../models/Cargo');
const Order = require('../models/Order');

const routes = {};

/*
 * ROUTES DEFINITION
 */

routes.estimate = (req, res) => {
	let body = req.body;
	let packagesMapResult;
	let packages;
	let cargo;

	packagesMapResult = Cargo.mapPackages(body);
	packages = packagesMapResult.packages;

	if(!packages || !packages.length){
		res.status(400);
		return res.end('Bad req');
	}

	cargo = Cargo.load(packages);

	Order.save(body, cargo);

	if(packagesMapResult.errors.packagesWeightExceeded.length || packagesMapResult.errors.packagesIncorrectInput.length)
		cargo.errors = packagesMapResult.errors;
	else
		delete cargo.errors;

	res.json(cargo);
}

routes.prices = (req, res) => {
	fs.exists(PDF_PATH_PRICES, (exist) => {
		if(!exist){
			res.status(404);
			console.log(PDF_PATH_PRICES);
			res.end('File not found');
			return;
		}

		fs.readFile(PDF_PATH_PRICES, (err, data) => {
			if(err){
				res.status(500);
				res.end('Server error while downloading file');
				return;
			}

			res.setHeader('Content-type', 'application/pdf');
			res.end(data);
		});
	});
}

/*
 * ROUTES BINDING
 */
router.post('/estimate', routes.estimate);
router.get('/prices', routes.prices);


module.exports = router;